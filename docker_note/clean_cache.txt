a. Hapus Container yang Berhenti:
docker ps -a | grep "Exited" | awk '{print $1}' | xargs docker rm

b. Hapus Gambar yang Tidak Terpakai:
docker images -q --filter "dangling=true" | xargs docker rmi

c. Hapus Volume yang Tidak Terpakai:
docker volume ls -qf dangling=true | xargs docker volume rm

d. Hapus Jaringan yang Tidak Terpakai:
docker network ls | grep "bridge" | awk '/ / { print $1 }' | xargs docker network rm